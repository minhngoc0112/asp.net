
/****** Object:  Database [ToyStore]    Script Date: 12/30/2021 9:12:32 PM ******/
CREATE DATABASE [ToyStore]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Toy', FILENAME = N'C:\Users\hoang\MSSQL15.MSSQLSERVER\MSSQL\DATA\Toy.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Toy_log', FILENAME = N'C:\Users\hoang\MSSQL15.MSSQLSERVER\MSSQL\DATA\Toy_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [ToyStore] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ToyStore].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ToyStore] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [ToyStore] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [ToyStore] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [ToyStore] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [ToyStore] SET ARITHABORT OFF 
GO
ALTER DATABASE [ToyStore] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [ToyStore] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [ToyStore] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [ToyStore] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [ToyStore] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [ToyStore] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [ToyStore] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [ToyStore] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [ToyStore] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [ToyStore] SET  DISABLE_BROKER 
GO
ALTER DATABASE [ToyStore] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [ToyStore] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [ToyStore] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [ToyStore] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [ToyStore] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [ToyStore] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [ToyStore] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [ToyStore] SET RECOVERY FULL 
GO
ALTER DATABASE [ToyStore] SET  MULTI_USER 
GO
ALTER DATABASE [ToyStore] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [ToyStore] SET DB_CHAINING OFF 
GO
ALTER DATABASE [ToyStore] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [ToyStore] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [ToyStore] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [ToyStore] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'ToyStore', N'ON'
GO
ALTER DATABASE [ToyStore] SET QUERY_STORE = OFF
GO
USE [ToyStore]
GO
/****** Object:  Table [dbo].[Accounts]    Script Date: 12/30/2021 9:12:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Accounts](
	[AccountID] [int] IDENTITY(1,1) NOT NULL,
	[Phone] [varchar](12) NULL,
	[Email] [nvarchar](50) NULL,
	[Password] [nvarchar](50) NULL,
	[Salt] [nchar](6) NULL,
	[Active] [bit] NOT NULL,
	[FullName] [nvarchar](150) NULL,
	[RoleID] [int] NULL,
	[LastLogin] [datetime] NULL,
	[CreatDate] [datetime] NULL,
 CONSTRAINT [PK_Accounts] PRIMARY KEY CLUSTERED 
(
	[AccountID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Attributes]    Script Date: 12/30/2021 9:12:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Attributes](
	[AttributeID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
 CONSTRAINT [PK_Attributes] PRIMARY KEY CLUSTERED 
(
	[AttributeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AttributesPrices]    Script Date: 12/30/2021 9:12:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AttributesPrices](
	[AttributesPriceID] [int] IDENTITY(1,1) NOT NULL,
	[AtributeID] [int] NULL,
	[ProductID] [int] NULL,
	[Price] [int] NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_AttributesPrices] PRIMARY KEY CLUSTERED 
(
	[AttributesPriceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Categories]    Script Date: 12/30/2021 9:12:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categories](
	[CatID] [int] IDENTITY(1,1) NOT NULL,
	[CatName] [nvarchar](250) NULL,
	[Description] [nchar](500) NULL,
	[ParentID] [int] NULL,
	[Levels] [int] NULL,
	[Ordering] [int] NULL,
	[Published] [bit] NOT NULL,
	[Thumb] [nvarchar](250) NULL,
	[Title] [nvarchar](250) NULL,
	[Alias] [nvarchar](250) NULL,
	[MetaDesc] [nvarchar](250) NULL,
	[MetaKey] [nvarchar](250) NULL,
	[Cover] [nvarchar](250) NULL,
	[SchemaMarkup] [nvarchar](500) NULL,
 CONSTRAINT [PK_Categories] PRIMARY KEY CLUSTERED 
(
	[CatID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customers]    Script Date: 12/30/2021 9:12:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customers](
	[CustomerID] [int] IDENTITY(1,1) NOT NULL,
	[FullName] [nvarchar](255) NULL,
	[Birthday] [datetime] NULL,
	[Avatar] [nvarchar](255) NULL,
	[Address] [nvarchar](255) NULL,
	[Email] [nchar](150) NULL,
	[Phone] [varchar](12) NULL,
	[LocationID] [int] NULL,
	[Dictrict] [nvarchar](500) NULL,
	[Ward] [nvarchar](500) NULL,
	[CreateDate] [datetime] NULL,
	[Password] [nvarchar](50) NULL,
	[Salt] [nchar](8) NULL,
	[LastLogin] [datetime] NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_Customers] PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Locations]    Script Date: 12/30/2021 9:12:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Locations](
	[LocationID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](20) NULL,
	[Type] [nvarchar](20) NULL,
	[Slug] [nvarchar](100) NULL,
	[NameWithType] [nvarchar](255) NULL,
	[PathWithType] [nvarchar](255) NULL,
	[ParentWithType] [nvarchar](255) NULL,
	[ParentCode] [int] NULL,
	[Levels] [int] NULL,
 CONSTRAINT [PK_Locations] PRIMARY KEY CLUSTERED 
(
	[LocationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderDetails]    Script Date: 12/30/2021 9:12:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderDetails](
	[OrderDetailID] [int] IDENTITY(1,1) NOT NULL,
	[OrderID] [int] NULL,
	[ProductID] [int] NULL,
	[OrderNumber] [int] NULL,
	[Quantity] [int] NULL,
	[Discount] [int] NULL,
	[TotalMoney] [int] NULL,
	[ShipDate] [datetime] NULL,
	[CreateDate] [datetime] NULL,
	[Amount] [int] NULL,
	[Price] [int] NULL,
 CONSTRAINT [PK_OrderDetails] PRIMARY KEY CLUSTERED 
(
	[OrderDetailID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Orders]    Script Date: 12/30/2021 9:12:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders](
	[OrderID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [int] NULL,
	[OrderDate] [datetime] NULL,
	[ShipDate] [datetime] NULL,
	[TransacStatusID] [int] NULL,
	[Deleted] [bit] NULL,
	[PaymentDate] [datetime] NULL,
	[PaymentID] [int] NULL,
	[Note] [nvarchar](max) NULL,
	[TotalMoney] [int] NULL,
	[LocationID] [int] NULL,
	[Dictrict] [nvarchar](max) NULL,
	[Ward] [nvarchar](max) NULL,
	[Address] [nvarchar](500) NULL,
 CONSTRAINT [PK_Orders] PRIMARY KEY CLUSTERED 
(
	[OrderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Page]    Script Date: 12/30/2021 9:12:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Page](
	[PageID] [int] IDENTITY(1,1) NOT NULL,
	[PageName] [nvarchar](250) NULL,
	[Contents] [nvarchar](max) NULL,
	[Thumb] [nvarchar](250) NULL,
	[Published] [bit] NOT NULL,
	[Title] [nvarchar](250) NULL,
	[MetaDesc] [nvarchar](250) NULL,
	[MetaKey] [nvarchar](250) NULL,
	[Alias] [nvarchar](25) NULL,
	[CreatedDate] [datetime] NULL,
	[Ordering] [int] NULL,
 CONSTRAINT [PK_Page] PRIMARY KEY CLUSTERED 
(
	[PageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Products]    Script Date: 12/30/2021 9:12:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Products](
	[ProductID] [int] IDENTITY(1,1) NOT NULL,
	[ProductName] [nvarchar](255) NULL,
	[ShortDesc] [nvarchar](255) NULL,
	[Description] [nvarchar](255) NULL,
	[CatID] [int] NULL,
	[Price] [int] NULL,
	[Discount] [int] NULL,
	[Thumb] [nvarchar](255) NULL,
	[Video] [nvarchar](255) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[BestSellers] [bit] NOT NULL,
	[HomeFlag] [bit] NOT NULL,
	[Active] [bit] NOT NULL,
	[Tags] [nvarchar](max) NULL,
	[Title] [nvarchar](255) NULL,
	[Alias] [nvarchar](255) NULL,
	[MeltaDesc] [nvarchar](255) NULL,
	[MeltaKey] [nvarchar](255) NULL,
	[UnsitslnStock] [int] NULL,
 CONSTRAINT [PK_Products] PRIMARY KEY CLUSTERED 
(
	[ProductID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 12/30/2021 9:12:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[RoleID] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [nvarchar](50) NULL,
	[Description] [nvarchar](50) NULL,
 CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED 
(
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Shippers]    Script Date: 12/30/2021 9:12:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Shippers](
	[ShipperID] [int] IDENTITY(1,1) NOT NULL,
	[ShipperName] [nvarchar](150) NULL,
	[Phone] [nchar](10) NULL,
	[Company] [nvarchar](150) NULL,
	[ShipDate] [datetime] NULL,
 CONSTRAINT [PK_Shippers] PRIMARY KEY CLUSTERED 
(
	[ShipperID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TinTucs]    Script Date: 12/30/2021 9:12:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TinTucs](
	[PostID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](255) NULL,
	[SContents] [nvarchar](255) NULL,
	[Thumb] [nvarchar](255) NULL,
	[Published] [bit] NOT NULL,
	[Alias] [nvarchar](255) NULL,
	[CreateDate] [datetime] NULL,
	[Author] [nvarchar](255) NULL,
	[AccountID] [int] NULL,
	[Tags] [nvarchar](max) NULL,
	[CartID] [int] NULL,
	[isHot] [bit] NULL,
	[isNewfeed] [bit] NULL,
	[MetaKey] [nvarchar](255) NULL,
	[MetaDesc] [nvarchar](255) NULL,
	[Views] [int] NULL,
 CONSTRAINT [PK_TinTucs] PRIMARY KEY CLUSTERED 
(
	[PostID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TransactStatus]    Script Date: 12/30/2021 9:12:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TransactStatus](
	[TransactStatusID] [int] IDENTITY(1,1) NOT NULL,
	[Status] [nvarchar](50) NULL,
	[Description] [nvarchar](max) NULL,
 CONSTRAINT [PK_TransactStatus] PRIMARY KEY CLUSTERED 
(
	[TransactStatusID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Categories] ON 

INSERT [dbo].[Categories] ([CatID], [CatName], [Description], [ParentID], [Levels], [Ordering], [Published], [Thumb], [Title], [Alias], [MetaDesc], [MetaKey], [Cover], [SchemaMarkup]) VALUES (1, N'Đồ chơi lắp ráp', NULL, NULL, NULL, NULL, 1, N'default.jpg', N'Đồ chơi lắp ráp', N'do-choi-lap-rap', N'Đồ chơi lắp ráp', N'Đồ chơi lắp ráp', NULL, NULL)
INSERT [dbo].[Categories] ([CatID], [CatName], [Description], [ParentID], [Levels], [Ordering], [Published], [Thumb], [Title], [Alias], [MetaDesc], [MetaKey], [Cover], [SchemaMarkup]) VALUES (2, N'Đồ chơi mô phỏng', N'Đồ chơi mô phỏng                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    ', NULL, NULL, NULL, 1, N'default.jpg', N'Đồ chơi mô phỏng', N'do-choi-mo-phong', N'Đồ chơi mô phỏng', N'Đồ chơi mô phỏng', NULL, NULL)
INSERT [dbo].[Categories] ([CatID], [CatName], [Description], [ParentID], [Levels], [Ordering], [Published], [Thumb], [Title], [Alias], [MetaDesc], [MetaKey], [Cover], [SchemaMarkup]) VALUES (3, N'Búp bê', N'Búp bê                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', NULL, NULL, NULL, 1, N'default.jpg', N'Búp bê', N'bup-be', N'Búp bê', N'Búp bê', NULL, NULL)
INSERT [dbo].[Categories] ([CatID], [CatName], [Description], [ParentID], [Levels], [Ordering], [Published], [Thumb], [Title], [Alias], [MetaDesc], [MetaKey], [Cover], [SchemaMarkup]) VALUES (4, N'Đồ chơi phương tiện', N'Đồ chơi phương tiện                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 ', NULL, NULL, NULL, 1, N'default.jpg', N'Đồ chơi phương tiện', N'do-choi-phuong-tien', N'Đồ chơi phương tiện', N'Đồ chơi phương tiện', NULL, NULL)
INSERT [dbo].[Categories] ([CatID], [CatName], [Description], [ParentID], [Levels], [Ordering], [Published], [Thumb], [Title], [Alias], [MetaDesc], [MetaKey], [Cover], [SchemaMarkup]) VALUES (5, N'Đồ chơi thời trang', N'Đồ chơi thời trang                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  ', NULL, NULL, NULL, 1, N'default.jpg', N'Đồ chơi thời trang', N'do-choi-thoi-trang', N'Đồ chơi thời trang', N'Đồ chơi thời trang', NULL, NULL)
SET IDENTITY_INSERT [dbo].[Categories] OFF
GO
SET IDENTITY_INSERT [dbo].[Customers] ON 

INSERT [dbo].[Customers] ([CustomerID], [FullName], [Birthday], [Avatar], [Address], [Email], [Phone], [LocationID], [Dictrict], [Ward], [CreateDate], [Password], [Salt], [LastLogin], [Active]) VALUES (1, N'Trần Thị Minh Ngọc', NULL, NULL, NULL, N'minhngoc012000@gmail.com                                                                                                                              ', N'0865315373', NULL, NULL, NULL, CAST(N'2021-12-29T23:38:21.607' AS DateTime), N'eb8f3c95f33dbc089462cadac40cc1c6', N'kp1is   ', NULL, 1)
SET IDENTITY_INSERT [dbo].[Customers] OFF
GO
SET IDENTITY_INSERT [dbo].[Page] ON 

INSERT [dbo].[Page] ([PageID], [PageName], [Contents], [Thumb], [Published], [Title], [MetaDesc], [MetaKey], [Alias], [CreatedDate], [Ordering]) VALUES (1, N'Hướng dẫn mua hàng', N'<ol class="X5LH0c" style="margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; color: rgb(32, 33, 36); font-family: arial, sans-serif; font-size: 16px;"><li class="TrT0Xe" style="margin: 0px 0px 4px; padding: 0px; list-style: inherit;">Bước 1: Chọn sản phẩm. Trên trang chủ www.<b>mykingdom</b>.com.vn Quý khách có thể dễ dàng tìm thấy sản phẩm mình muốn&nbsp;<b>mua</b>&nbsp;một cách thuận tiện và nhanh chóng&nbsp;</li><li class="TrT0Xe" style="margin: 0px 0px 4px; padding: 0px; list-style: inherit;">Bước 2: Cho vào Giỏ&nbsp;<b>Hàng</b>. Với mỗi sản phẩm</li><li class="TrT0Xe" style="margin: 0px 0px 4px; padding: 0px; list-style: inherit;">Bước 3: Thanh toán.</li></ol>', N'huong-dan-mua-hang.jpg', 1, N'Hướng dẫn mua hàng', N'Hướng dẫn mua hàng', N'Hướng dẫn mua hàng', N'huong-dan-mua-hang', NULL, NULL)
INSERT [dbo].[Page] ([PageID], [PageName], [Contents], [Thumb], [Published], [Title], [MetaDesc], [MetaKey], [Alias], [CreatedDate], [Ordering]) VALUES (2, N'chính sách và điều khoản', NULL, N'chinh-sach-va-dieu-khoan.png', 1, NULL, NULL, NULL, N'chinh-sach-va-dieu-khoan', NULL, NULL)
SET IDENTITY_INSERT [dbo].[Page] OFF
GO
SET IDENTITY_INSERT [dbo].[Products] ON 

INSERT [dbo].[Products] ([ProductID], [ProductName], [ShortDesc], [Description], [CatID], [Price], [Discount], [Thumb], [Video], [DateCreated], [DateModified], [BestSellers], [HomeFlag], [Active], [Tags], [Title], [Alias], [MeltaDesc], [MeltaKey], [UnsitslnStock]) VALUES (2, N'Tranh Lắp Ráp LEGO Marvel Studios Iron Man', N'Tranh Lắp Ráp LEGO Marvel Studios Iron Man', N'Người sắt của Marvel Studios Bạn đã bao giờ mơ ước được sở hữu một bộ áo giáp của Người Sắt? ', 1, 4399000, NULL, N'tranh-lap-rap-lego-marvel-studios-iron-man.jpg', NULL, CAST(N'2021-12-29T17:18:52.100' AS DateTime), CAST(N'2021-12-29T17:18:52.100' AS DateTime), 1, 1, 1, NULL, N'Tranh Lắp Ráp LEGO Marvel Studios Iron Man', N'tranh-lap-rap-lego-marvel-studios-iron-man', N'Tranh Lắp Ráp LEGO Marvel Studios Iron Man', N'Tranh Lắp Ráp LEGO Marvel Studios Iron Man', 100)
INSERT [dbo].[Products] ([ProductID], [ProductName], [ShortDesc], [Description], [CatID], [Price], [Discount], [Thumb], [Video], [DateCreated], [DateModified], [BestSellers], [HomeFlag], [Active], [Tags], [Title], [Alias], [MeltaDesc], [MeltaKey], [UnsitslnStock]) VALUES (3, N'Bộ Lắp Ráp Điều Hành Trạm Sửa Xe', N'Bộ lắp ráp điều hành trạm sửa xe', N'Bộ sản phẩm sẽ bao gồm: Các mảnh ghép của hệ thống trạm sửa xe, 6 xe con, bộ sticker đầy cảm hứng và các vật trang trí đi kèm.', 1, 475000, NULL, N'bo-lap-rap-dieu-hanh-tram-sua-xe.jpg', NULL, CAST(N'2021-12-29T17:30:28.843' AS DateTime), CAST(N'2021-12-29T17:30:27.980' AS DateTime), 1, 1, 1, NULL, N'Bộ lắp ráp điều hành trạm sửa xe', N'bo-lap-rap-dieu-hanh-tram-sua-xe', N'Bộ lắp ráp điều hành trạm sửa xe', N'Bộ lắp ráp điều hành trạm sửa xe', 100)
INSERT [dbo].[Products] ([ProductID], [ProductName], [ShortDesc], [Description], [CatID], [Price], [Discount], [Thumb], [Video], [DateCreated], [DateModified], [BestSellers], [HomeFlag], [Active], [Tags], [Title], [Alias], [MeltaDesc], [MeltaKey], [UnsitslnStock]) VALUES (4, N'Garage Hiện Đại 3 Trong 1', N'Garage hiện đại 3 trong 1', N'đồ chơi mô hình garage hiện đại với một chiếc xe tải Abrick đến từ thương hiệu Écoiffier', 1, 1159000, NULL, N'bo-lap-rap-dieu-hanh-tram-sua-xe.jpg', NULL, CAST(N'2021-12-29T17:31:20.000' AS DateTime), CAST(N'2021-12-29T17:41:51.593' AS DateTime), 1, 1, 1, NULL, N'Garage hiện đại 3 trong 1', N'garage-hien-dai-3-trong-1', N'Garage hiện đại 3 trong 1', N'Garage hiện đại 3 trong 1', 100)
INSERT [dbo].[Products] ([ProductID], [ProductName], [ShortDesc], [Description], [CatID], [Price], [Discount], [Thumb], [Video], [DateCreated], [DateModified], [BestSellers], [HomeFlag], [Active], [Tags], [Title], [Alias], [MeltaDesc], [MeltaKey], [UnsitslnStock]) VALUES (5, N'Nhân Vật Baby Yoda', N'Nhân Vật Baby Yoda', N'Xây dựng mô hình xây dựng dễ thương của riêng bạn của Child from Star Wars: The Mandalorian!', 1, 2666000, NULL, N'nhan-vat-baby-yoda.jpg', NULL, CAST(N'2021-12-29T17:31:56.000' AS DateTime), CAST(N'2021-12-29T17:46:30.610' AS DateTime), 1, 1, 1, NULL, N'Nhân Vật Baby Yoda', N'nhan-vat-baby-yoda', N'Nhân Vật Baby Yoda', N'Nhân Vật Baby Yoda', 100)
INSERT [dbo].[Products] ([ProductID], [ProductName], [ShortDesc], [Description], [CatID], [Price], [Discount], [Thumb], [Video], [DateCreated], [DateModified], [BestSellers], [HomeFlag], [Active], [Tags], [Title], [Alias], [MeltaDesc], [MeltaKey], [UnsitslnStock]) VALUES (6, N'Bộ Bàn Ghế Trang Điểm Hồng Đáng Yêu', N'Bộ bàn ghế trang điểm hồng đáng yêu', N'Bộ sản phẩm gồm 28 chi tiết .Có đèn và âm thanh, 5 bài hát tự động phát nhờ cảm ứng ánh sáng ở gương, 3 hộc bàn cho bé đựng đồ dùng trang điểm', 5, 489000, NULL, N'bo-ban-ghe-trang-diem-hong-dang-yeu.jpg', NULL, CAST(N'2021-12-29T17:50:18.117' AS DateTime), CAST(N'2021-12-29T17:50:18.117' AS DateTime), 1, 1, 1, NULL, N'Bộ bàn ghế trang điểm hồng đáng yêu', N'bo-ban-ghe-trang-diem-hong-dang-yeu', N'Bộ bàn ghế trang điểm hồng đáng yêu', N'Bộ bàn ghế trang điểm hồng đáng yêu', 100)
INSERT [dbo].[Products] ([ProductID], [ProductName], [ShortDesc], [Description], [CatID], [Price], [Discount], [Thumb], [Video], [DateCreated], [DateModified], [BestSellers], [HomeFlag], [Active], [Tags], [Title], [Alias], [MeltaDesc], [MeltaKey], [UnsitslnStock]) VALUES (7, N'Bộ Đồ Chơi Bác Sĩ', N'Bộ đồ chơi bác sĩ', N'Bộ đồ chơi bác sĩ Battat BT2437Z cho phép bé được nhập vai làm bác sĩ, với đầy đủ các dụng cụ y tế cần thiết.', 2, 419000, NULL, N'bo-do-choi-bac-si.jpg', NULL, CAST(N'2021-12-29T18:09:22.793' AS DateTime), CAST(N'2021-12-29T18:09:22.783' AS DateTime), 1, 1, 1, NULL, N'Bộ đồ chơi bác sĩ', N'bo-do-choi-bac-si', N'Bộ đồ chơi bác sĩ', N'Bộ đồ chơi bác sĩ', 100)
INSERT [dbo].[Products] ([ProductID], [ProductName], [ShortDesc], [Description], [CatID], [Price], [Discount], [Thumb], [Video], [DateCreated], [DateModified], [BestSellers], [HomeFlag], [Active], [Tags], [Title], [Alias], [MeltaDesc], [MeltaKey], [UnsitslnStock]) VALUES (8, N'Bồn Rửa Mặt Mini ', N'Bồn rửa mặt mini ', N'Bồn rửa mặt mini xinh xắn có 2 màu đỏ và xanh, với 12 chi tiết kèm theo cho bé tha hồ khám phá.', 5, 287000, NULL, N'bon-rua-mat-mini.jpg', NULL, CAST(N'2021-12-29T18:11:12.260' AS DateTime), CAST(N'2021-12-29T18:11:12.260' AS DateTime), 1, 1, 1, NULL, N'Bồn rửa mặt mini ', N'bon-rua-mat-mini', N'Bồn rửa mặt mini ', N'Bồn rửa mặt mini ', 100)
INSERT [dbo].[Products] ([ProductID], [ProductName], [ShortDesc], [Description], [CatID], [Price], [Discount], [Thumb], [Video], [DateCreated], [DateModified], [BestSellers], [HomeFlag], [Active], [Tags], [Title], [Alias], [MeltaDesc], [MeltaKey], [UnsitslnStock]) VALUES (9, N'Bộ Phòng Chơi Nổi 3D - Tiệm Tạp Hóa Nhỏ Xinh Của Anna', N'Bộ phòng chơi nổi 3D - Tiệm tạp hóa nhỏ xinh của Anna', N'Trẻ em sẽ vô cùng thích thú khi mở chiếc hộp đựng màu đỏ và xem những cảnh tượng 3D được hiện ra ngay trước mắt', 2, 599000, NULL, N'bo-phong-choi-noi-3d-tiem-tap-hoa-nho-xinh-cua-anna.jpg', NULL, CAST(N'2021-12-29T18:14:57.663' AS DateTime), CAST(N'2021-12-29T18:14:57.663' AS DateTime), 0, 1, 1, NULL, N'Bộ phòng chơi nổi 3D - Tiệm tạp hóa nhỏ xinh của Anna', N'bo-phong-choi-noi-3d-tiem-tap-hoa-nho-xinh-cua-anna', N'Bộ phòng chơi nổi 3D - Tiệm tạp hóa nhỏ xinh của Anna', N'Bộ phòng chơi nổi 3D - Tiệm tạp hóa nhỏ xinh của Anna', 100)
INSERT [dbo].[Products] ([ProductID], [ProductName], [ShortDesc], [Description], [CatID], [Price], [Discount], [Thumb], [Video], [DateCreated], [DateModified], [BestSellers], [HomeFlag], [Active], [Tags], [Title], [Alias], [MeltaDesc], [MeltaKey], [UnsitslnStock]) VALUES (10, N'Bộ Thiết Kế Vòng Tay Công Chúa Disney X Juicy Couture', N'Bộ thiết kế vòng tay Công Chúa Disney x Juicy Couture', N'Bộ thiết kế vòng tay Công Chúa Disney x Juicy Couture', 5, 399000, NULL, N'bo-thiet-ke-vong-tay-cong-chua-disney-x-juicy-couture.jpg', NULL, CAST(N'2021-12-29T18:16:14.220' AS DateTime), CAST(N'2021-12-29T18:16:14.220' AS DateTime), 0, 1, 1, NULL, N'Bộ thiết kế vòng tay Công Chúa Disney x Juicy Couture', N'bo-thiet-ke-vong-tay-cong-chua-disney-x-juicy-couture', N'Bộ thiết kế vòng tay Công Chúa Disney x Juicy Couture', N'Bộ thiết kế vòng tay Công Chúa Disney x Juicy Couture', 100)
INSERT [dbo].[Products] ([ProductID], [ProductName], [ShortDesc], [Description], [CatID], [Price], [Discount], [Thumb], [Video], [DateCreated], [DateModified], [BestSellers], [HomeFlag], [Active], [Tags], [Title], [Alias], [MeltaDesc], [MeltaKey], [UnsitslnStock]) VALUES (11, N'Chiến Giáp Người Sắt', N'Chiến Giáp Người Sắt', N'Tham gia cùng Iron Man để bảo vệ thế giới khỏi những kẻ xấu xa và những kẻ xâm lược ngoài hành tinh! Cần thêm hỏa lực? ', 1, 299000, NULL, N'chien-giap-nguoi-sat.jpg', NULL, CAST(N'2021-12-29T18:17:49.960' AS DateTime), CAST(N'2021-12-29T18:17:49.960' AS DateTime), 1, 1, 1, NULL, N'Chiến Giáp Người Sắt', N'chien-giap-nguoi-sat', N'Chiến Giáp Người Sắt', N'Chiến Giáp Người Sắt', 100)
INSERT [dbo].[Products] ([ProductID], [ProductName], [ShortDesc], [Description], [CatID], [Price], [Discount], [Thumb], [Video], [DateCreated], [DateModified], [BestSellers], [HomeFlag], [Active], [Tags], [Title], [Alias], [MeltaDesc], [MeltaKey], [UnsitslnStock]) VALUES (12, N'Chiến Xe Nhào Lộn Monster Jam', N'Chiến xe nhào lộn Monster Jam', N'Chiến xe biểu diễn nhào lộn Monster Jam với tỷ lệ 1:15 từ chiến xe thật trên đấu trường rực lửa lần đầu tiên xuất hiện tại Việt Nam.', 4, 1399000, NULL, N'chien-xe-nhao-lon-monster-jam.jpg', NULL, CAST(N'2021-12-29T18:19:23.000' AS DateTime), CAST(N'2021-12-29T18:19:58.173' AS DateTime), 1, 1, 1, NULL, N'Chiến xe nhào lộn Monster Jam', N'chien-xe-nhao-lon-monster-jam', N'Chiến xe nhào lộn Monster Jam', N'Chiến xe nhào lộn Monster Jam', 100)
INSERT [dbo].[Products] ([ProductID], [ProductName], [ShortDesc], [Description], [CatID], [Price], [Discount], [Thumb], [Video], [DateCreated], [DateModified], [BestSellers], [HomeFlag], [Active], [Tags], [Title], [Alias], [MeltaDesc], [MeltaKey], [UnsitslnStock]) VALUES (13, N'Chuồng Ngựa', N'Chuồng Ngựa', N'LEGO Minecraft 21171 Chuồng Ngựa (241 chi tiết) kết hợp tất cả niềm vui và cuộc phiêu lưu của trò chơi trực tuyến phổ biến với khả năng sáng tạo vô tận của những viên gạch LEGO và niềm vui khi chăm sóc những chú ngựa.', 2, 799000, NULL, N'chuong-ngua.jpg', NULL, CAST(N'2021-12-29T18:21:14.673' AS DateTime), CAST(N'2021-12-29T18:21:14.673' AS DateTime), 0, 1, 1, NULL, N'Chuồng Ngựa', N'chuong-ngua', N'Chuồng Ngựa', N'Chuồng Ngựa', 100)
INSERT [dbo].[Products] ([ProductID], [ProductName], [ShortDesc], [Description], [CatID], [Price], [Discount], [Thumb], [Video], [DateCreated], [DateModified], [BestSellers], [HomeFlag], [Active], [Tags], [Title], [Alias], [MeltaDesc], [MeltaKey], [UnsitslnStock]) VALUES (14, N'Công Chúa Ariel Biết Hát', N'Công chúa Ariel biết hát', N'Nhấn vào vỏ sò trên người của Ariel, vỏ sò sẽ sáng và cô ấy sẽ hát vang bài hát huyền thoại "Part of your world" trong bộ phim Nàng tiên cá Ariel', 3, 799000, NULL, N'cong-chua-ariel-biet-hat.jpg', NULL, CAST(N'2021-12-29T18:22:41.920' AS DateTime), CAST(N'2021-12-29T18:22:41.920' AS DateTime), 0, 1, 1, NULL, N'Công chúa Ariel biết hát', N'cong-chua-ariel-biet-hat', N'Công chúa Ariel biết hát', N'Công chúa Ariel biết hát', 100)
INSERT [dbo].[Products] ([ProductID], [ProductName], [ShortDesc], [Description], [CatID], [Price], [Discount], [Thumb], [Video], [DateCreated], [DateModified], [BestSellers], [HomeFlag], [Active], [Tags], [Title], [Alias], [MeltaDesc], [MeltaKey], [UnsitslnStock]) VALUES (15, N'Công Chúa Ariel Và Chiếc Váy Diệu Kỳ', N'Công chúa Ariel và chiếc váy diệu kỳ', N'THỜI TRANG CHIẾC VÁY DIỆU KỲ ARIEL: Mở ra một thế giới tưởng tượng với bộ đồ chơi Công chúa Disney Ariel, nữ anh hùng dũng cảm trong bộ phim kinh điển Nàng tiên cá của Disney', 3, 319000, NULL, N'cong-chua-ariel-va-chiec-vay-dieu-ky.jpg', NULL, CAST(N'2021-12-29T18:24:22.117' AS DateTime), CAST(N'2021-12-29T18:24:22.117' AS DateTime), 0, 1, 1, NULL, N'Công chúa Ariel và chiếc váy diệu kỳ', N'cong-chua-ariel-va-chiec-vay-dieu-ky', N'Công chúa Ariel và chiếc váy diệu kỳ', N'Công chúa Ariel và chiếc váy diệu kỳ', 100)
INSERT [dbo].[Products] ([ProductID], [ProductName], [ShortDesc], [Description], [CatID], [Price], [Discount], [Thumb], [Video], [DateCreated], [DateModified], [BestSellers], [HomeFlag], [Active], [Tags], [Title], [Alias], [MeltaDesc], [MeltaKey], [UnsitslnStock]) VALUES (16, N'Đội Trưởng Carter & Chiến Giáp Hydra Stomper', N'Đội trưởng Carter & chiến giáp Hydra Stomper', N'Đội trưởng Carter & chiến giáp Hydra Stomper', 2, 999000, NULL, N'doi-truong-carter-chien-giap-hydra-stomper.jpg', NULL, CAST(N'2021-12-29T18:25:55.050' AS DateTime), CAST(N'2021-12-29T18:25:55.050' AS DateTime), 0, 1, 1, NULL, N'Đội trưởng Carter & chiến giáp Hydra Stomper', N'doi-truong-carter-chien-giap-hydra-stomper', N'Đội trưởng Carter & chiến giáp Hydra Stomper', N'Đội trưởng Carter & chiến giáp Hydra Stomper', 100)
INSERT [dbo].[Products] ([ProductID], [ProductName], [ShortDesc], [Description], [CatID], [Price], [Discount], [Thumb], [Video], [DateCreated], [DateModified], [BestSellers], [HomeFlag], [Active], [Tags], [Title], [Alias], [MeltaDesc], [MeltaKey], [UnsitslnStock]) VALUES (17, N'HộP Phụ Kiện Mèo Con Bí Ẩn', N'HộP Phụ Kiện Mèo Con Bí Ẩn', N'Truyền cảm hứng cho những bạn trẻ yêu thích đồ thủ công hoặc động vật với bộ LEGO DOTS 41924 Hộp Phụ Kiện Mèo Con Bí Ẩn ( 451 Chi tiết) thú vị này!', 1, 419000, NULL, N'hop-phu-kien-meo-con-bi-an.jpg', NULL, CAST(N'2021-12-29T18:27:38.737' AS DateTime), CAST(N'2021-12-29T18:27:38.737' AS DateTime), 1, 1, 1, NULL, N'HộP Phụ Kiện Mèo Con Bí Ẩn', N'hop-phu-kien-meo-con-bi-an', N'HộP Phụ Kiện Mèo Con Bí Ẩn', N'HộP Phụ Kiện Mèo Con Bí Ẩn', 100)
INSERT [dbo].[Products] ([ProductID], [ProductName], [ShortDesc], [Description], [CatID], [Price], [Discount], [Thumb], [Video], [DateCreated], [DateModified], [BestSellers], [HomeFlag], [Active], [Tags], [Title], [Alias], [MeltaDesc], [MeltaKey], [UnsitslnStock]) VALUES (18, N'Lâu Đài Băng Giá', N'Lâu đài băng giá', N'Lâu đài băng giá', 1, 6999000, NULL, N'lau-dai-bang-gia.jpg', NULL, CAST(N'2021-12-29T22:07:34.893' AS DateTime), CAST(N'2021-12-29T22:07:34.893' AS DateTime), 1, 1, 1, NULL, N'Lâu đài băng giá', N'lau-dai-bang-gia', N'Lâu đài băng giá', N'Lâu đài băng giá', 10)
INSERT [dbo].[Products] ([ProductID], [ProductName], [ShortDesc], [Description], [CatID], [Price], [Discount], [Thumb], [Video], [DateCreated], [DateModified], [BestSellers], [HomeFlag], [Active], [Tags], [Title], [Alias], [MeltaDesc], [MeltaKey], [UnsitslnStock]) VALUES (19, N'Máy Chụp Hình Chống Nước Vàng Cá Tính', N'Máy chụp hình chống nước Vàng cá tính', N'21 khung ảnh vui nhộn cùng camera kép, độ phân giải 20M/12M/10M sẵn sàng cùng bé ghi lại những khoảnh khắc vui chơi cùng người thân và bạn bè.', 5, 699000, NULL, N'may-chup-hinh-chong-nuoc-vang-ca-tinh.png', NULL, CAST(N'2021-12-29T22:09:14.880' AS DateTime), CAST(N'2021-12-29T22:09:14.880' AS DateTime), 0, 1, 1, NULL, N'Máy chụp hình chống nước Vàng cá tính', N'may-chup-hinh-chong-nuoc-vang-ca-tinh', N'Máy chụp hình chống nước Vàng cá tính', N'Máy chụp hình chống nước Vàng cá tính', 100)
INSERT [dbo].[Products] ([ProductID], [ProductName], [ShortDesc], [Description], [CatID], [Price], [Discount], [Thumb], [Video], [DateCreated], [DateModified], [BestSellers], [HomeFlag], [Active], [Tags], [Title], [Alias], [MeltaDesc], [MeltaKey], [UnsitslnStock]) VALUES (20, N'Máy Chụp Hình Phun Bong Bóng - Thỏ Con Đáng Yêu', N'Máy chụp hình phun bong bóng - Thỏ con đáng yêu', N'Món đồ chơi này vừa có thể tạo bong bóng, vừa phát nhạc nhưng lại cực kỳ an toàn', 5, 149000, NULL, N'may-chup-hinh-phun-bong-bong-tho-con-dang-yeu.jpg', NULL, CAST(N'2021-12-29T22:10:31.130' AS DateTime), CAST(N'2021-12-29T22:10:31.130' AS DateTime), 0, 1, 1, NULL, N'Máy chụp hình phun bong bóng - Thỏ con đáng yêu', N'may-chup-hinh-phun-bong-bong-tho-con-dang-yeu', N'Máy chụp hình phun bong bóng - Thỏ con đáng yêu', N'Máy chụp hình phun bong bóng - Thỏ con đáng yêu', 100)
INSERT [dbo].[Products] ([ProductID], [ProductName], [ShortDesc], [Description], [CatID], [Price], [Discount], [Thumb], [Video], [DateCreated], [DateModified], [BestSellers], [HomeFlag], [Active], [Tags], [Title], [Alias], [MeltaDesc], [MeltaKey], [UnsitslnStock]) VALUES (21, N'Phụ Kiện Móc Khóa Rồng Con', N'Phụ kiện móc khóa rồng con', N'Phụ kiện móc khóa rồng con', 5, 199000, NULL, N'phu-kien-moc-khoa-rong-con.jpg', NULL, CAST(N'2021-12-29T22:12:18.943' AS DateTime), CAST(N'2021-12-29T22:12:18.943' AS DateTime), 0, 1, 1, NULL, N'Phụ kiện móc khóa rồng con', N'phu-kien-moc-khoa-rong-con', N'Phụ kiện móc khóa rồng con', N'Phụ kiện móc khóa rồng con', 100)
INSERT [dbo].[Products] ([ProductID], [ProductName], [ShortDesc], [Description], [CatID], [Price], [Discount], [Thumb], [Video], [DateCreated], [DateModified], [BestSellers], [HomeFlag], [Active], [Tags], [Title], [Alias], [MeltaDesc], [MeltaKey], [UnsitslnStock]) VALUES (22, N'Trạm Cứu Hỏa Abrick', N'Trạm Cứu Hỏa Abrick', N'mô hình đồ chơi mô phỏng trạm cứu hỏa thực tế - một doanh trại hoàn chỉnh với đội ngũ lính cứu hỏa có trình độ cho tất cả các biện pháp can thiệp: trên không, trên đường …', 2, 999000, NULL, N'tram-cuu-hoa-abrick.jpg', NULL, CAST(N'2021-12-29T22:13:55.363' AS DateTime), CAST(N'2021-12-29T22:13:55.363' AS DateTime), 0, 1, 1, NULL, N'Trạm Cứu Hỏa Abrick', N'tram-cuu-hoa-abrick', N'Trạm Cứu Hỏa Abrick', N'Trạm Cứu Hỏa Abrick', 100)
SET IDENTITY_INSERT [dbo].[Products] OFF
GO
SET IDENTITY_INSERT [dbo].[Roles] ON 

INSERT [dbo].[Roles] ([RoleID], [RoleName], [Description]) VALUES (1, N'Admin', N'Quản trị viên')
INSERT [dbo].[Roles] ([RoleID], [RoleName], [Description]) VALUES (2, N'Staff', N'Nhân viên')
SET IDENTITY_INSERT [dbo].[Roles] OFF
GO
ALTER TABLE [dbo].[Accounts]  WITH CHECK ADD  CONSTRAINT [FK_Accounts_Roles] FOREIGN KEY([RoleID])
REFERENCES [dbo].[Roles] ([RoleID])
GO
ALTER TABLE [dbo].[Accounts] CHECK CONSTRAINT [FK_Accounts_Roles]
GO
ALTER TABLE [dbo].[AttributesPrices]  WITH CHECK ADD  CONSTRAINT [FK_AttributesPrices_Attributes] FOREIGN KEY([AtributeID])
REFERENCES [dbo].[Attributes] ([AttributeID])
GO
ALTER TABLE [dbo].[AttributesPrices] CHECK CONSTRAINT [FK_AttributesPrices_Attributes]
GO
ALTER TABLE [dbo].[AttributesPrices]  WITH CHECK ADD  CONSTRAINT [FK_AttributesPrices_Products] FOREIGN KEY([ProductID])
REFERENCES [dbo].[Products] ([ProductID])
GO
ALTER TABLE [dbo].[AttributesPrices] CHECK CONSTRAINT [FK_AttributesPrices_Products]
GO
ALTER TABLE [dbo].[Customers]  WITH CHECK ADD  CONSTRAINT [FK_Customers_Locations] FOREIGN KEY([LocationID])
REFERENCES [dbo].[Locations] ([LocationID])
GO
ALTER TABLE [dbo].[Customers] CHECK CONSTRAINT [FK_Customers_Locations]
GO
ALTER TABLE [dbo].[OrderDetails]  WITH CHECK ADD  CONSTRAINT [FK_OrderDetails_Orders] FOREIGN KEY([OrderID])
REFERENCES [dbo].[Orders] ([OrderID])
GO
ALTER TABLE [dbo].[OrderDetails] CHECK CONSTRAINT [FK_OrderDetails_Orders]
GO
ALTER TABLE [dbo].[OrderDetails]  WITH CHECK ADD  CONSTRAINT [FK_OrderDetails_Products] FOREIGN KEY([ProductID])
REFERENCES [dbo].[Products] ([ProductID])
GO
ALTER TABLE [dbo].[OrderDetails] CHECK CONSTRAINT [FK_OrderDetails_Products]
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_Customers] FOREIGN KEY([CustomerID])
REFERENCES [dbo].[Customers] ([CustomerID])
GO
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_Customers]
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_Locations] FOREIGN KEY([LocationID])
REFERENCES [dbo].[Locations] ([LocationID])
GO
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_Locations]
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_TransactStatus] FOREIGN KEY([TransacStatusID])
REFERENCES [dbo].[TransactStatus] ([TransactStatusID])
GO
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_TransactStatus]
GO
ALTER TABLE [dbo].[Products]  WITH CHECK ADD  CONSTRAINT [FK_Products_Categories] FOREIGN KEY([CatID])
REFERENCES [dbo].[Categories] ([CatID])
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [FK_Products_Categories]
GO
ALTER TABLE [dbo].[TinTucs]  WITH CHECK ADD  CONSTRAINT [FK_TinTucs_Accounts] FOREIGN KEY([AccountID])
REFERENCES [dbo].[Accounts] ([AccountID])
GO
ALTER TABLE [dbo].[TinTucs] CHECK CONSTRAINT [FK_TinTucs_Accounts]
GO
USE [master]
GO
ALTER DATABASE [ToyStore] SET  READ_WRITE 
GO
USE [ToyStore]
GO
ALTER TABLE [dbo].[TinTucs] DROP CONSTRAINT [FK_TinTucs_Accounts]
GO
ALTER TABLE [dbo].[Products] DROP CONSTRAINT [FK_Products_Categories]
GO
ALTER TABLE [dbo].[Orders] DROP CONSTRAINT [FK_Orders_TransactStatus]
GO
ALTER TABLE [dbo].[Orders] DROP CONSTRAINT [FK_Orders_Locations]
GO
ALTER TABLE [dbo].[Orders] DROP CONSTRAINT [FK_Orders_Customers]
GO
ALTER TABLE [dbo].[OrderDetails] DROP CONSTRAINT [FK_OrderDetails_Products]
GO
ALTER TABLE [dbo].[OrderDetails] DROP CONSTRAINT [FK_OrderDetails_Orders]
GO
ALTER TABLE [dbo].[Customers] DROP CONSTRAINT [FK_Customers_Locations]
GO
ALTER TABLE [dbo].[AttributesPrices] DROP CONSTRAINT [FK_AttributesPrices_Products]
GO
ALTER TABLE [dbo].[AttributesPrices] DROP CONSTRAINT [FK_AttributesPrices_Attributes]
GO
ALTER TABLE [dbo].[Accounts] DROP CONSTRAINT [FK_Accounts_Roles]
GO