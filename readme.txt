Hướng dẫn chạy demo:
- Đầu tiên chạy file srcipt.sql để thêm database vào mysql server. Sau đó, ta cần xây dựng data diagrams bằng cách nối các mối
  quan hệ khóa chính khóa phụ giữa các bảng với nhau, bằng cách:
	B1: Nhấn chuột phải vào Database Diagrams từ database ToyStore ta vừa tạo. Chọn mục New Database Diagram.
	B2: Add tất cả các bảng hiện có.
	B3: Nối các khóa chính và khóa phụ giữa các bảng như trong video demo.
	B4: Save Diagram vừa tạo.
- Chạy file ToyStore.sln bằng microsoft visual studio
- Trong thư mục appsettings.json đổi tên server trùng với tên server của Microsoft SQL Server.
- Run project bằng cách nhán nút IIS Express trên thanh công cụ.

		
	
